//: Playground - noun: a place where people can play

class Animal {
    var name:String
    var age:Int
    var energy = 100
    var strength = 100
    
    init(name:String, age:Int) {
        self.name = name
        self.age = age
        if age < 1 {
            energy = 50
        }
    }
    
    func walk(steps:Int) {
        println("I am going forward by \(steps)")
        energy = energy - steps*2
    }
}


class Dog: Animal {
    var breed: String
    
    init(name:String, age:Int, breed:String){
        self.breed = breed
        super.init(name: name, age: age)
    }
    
    func eat(numberOfLegPeices:Int) {
        energy = energy + (numberOfLegPeices*10)
        
    }
    
    func bark(enemy:Dog) {
        println("wuff wuff")
        energy = energy - 5

        switch (breed) {
            case "Bulldog":
            enemy.strength = enemy.strength - 10
            case "Desi":
            enemy.strength = enemy.strength - 2
        default:
            enemy.strength = enemy.strength - 5
        }
        
    }
    
    func bite(enemy:Dog) {
        //TODO: implement this function
    }
}

class Elephant: Animal {
    var hasTeeths:Bool
    
    init(name:String, age:Int, hasTeeths:Bool){
        self.hasTeeths = hasTeeths
        super.init(name: name, age: age)
    }
    
}


class Kangaroo: Animal {
    override func walk(steps: Int) {
        println("I am jumping forward by \(steps)")
        energy = energy - steps*3
    }

}

var parvindersDog = Dog(name: "tomy", age: 2, breed: "Bulldog")
var vikrantsDog = Dog(name: "fuukhSingh", age: 2, breed: "Desi")

var dagarsElepaht = Elephant(name: "Elephanto", age: 4, hasTeeths: false)
dagarsElepaht.walk(30)
parvindersDog.eat(4)

var gauravsKangaroo = Kangaroo(name: "bijnori", age: 2)
gauravsKangaroo.walk(2)



parvindersDog.bark(vikrantsDog)


parvindersDog.bark(vikrantsDog)

vikrantsDog.bark(parvindersDog)
vikrantsDog.bark(parvindersDog)

parvindersDog



class User {
    var firstName:String
    var lastName:String
    var email:String
    var phoneNumber:String
    var points:Int
    
    init(firstName:String, lastName:String, email:String, phoneNumber:String, points:Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phoneNumber = phoneNumber
        self.points = points
    }
}

var parvinder = User(firstName: "Parvindra", lastName: "Singh", email: "abcd@example.com", phoneNumber: "+91999999999", points: 100)


parvinder.firstName + " " + parvinder.lastName
parvinder.phoneNumber

parvinder.fullname = "abcd"

var userInfo = User(

var parvinderDict = [
    "fullname": "Parvindra Singh",
    "email_addr": "qwerty@example.com",
    "phone_number": "+9199999999"

]

    var anotherDict = [
        "fullname": "Parvindra Singh",
        "email": "qwerty@example.com",
        "phone_number": "+9199999999"
        
]


func barkWithoutOpps(myDogEnergy:Int, myDogBreed:String, enemyDogStrength:Int) {
    var myEnergy = myDogEnergy - 5
    var enemyStrength:Int
    
    switch (myDogBreed) {
    case "Bulldog":
        enemyStrength = enemyDogStrength - 10
    case "Desi":
        enemyStrength = enemyDogStrength - 2
    default:
        enemyStrength = enemyDogStrength - 5
    }
}



var myFightResult = barkWithoutOpps(10, "bullDog", 20)












func aFunctionWhichReturnsFunction() -> ([Int] -> (Int, Int)) {
    func highestAndSecondHighest(numbersArray:[Int]) -> (max:Int, secondMax:Int) {
        
        var maxNumber = 0
        var secondMax = 0
        
        for number in numbersArray {
            if number > maxNumber {
                secondMax = maxNumber
                maxNumber = number
            } else if number > secondMax {
                secondMax = number
            }
        }
        
        return (maxNumber, secondMax);
        
    }
    
    func add(firstNumber:Int, secondNumber:Int) -> Int {
        return firstNumber + secondNumber
    }
    
    return highestAndSecondHighest
}



var myVariable = aFunctionWhichReturnsFunction()

myVariable([20, 10, 4, 8, 13])

func bekarFunction(neverUserparameter:Int) -> String {
    return "Hello world!"
}

var someString = "Hello world!"













































